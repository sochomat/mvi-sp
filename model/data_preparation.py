import sys
import argparse
import numpy as np

parser = argparse.ArgumentParser()

parser.add_argument('--input', help='Input file, one title per line', default="data.txt")
parser.add_argument('--output_words', help='Output file for words', default="words_processed.txt")
parser.add_argument('--output_titles', help='Output file for titles', default="titles_processed.txt")

args = vars(parser.parse_args())

input_filename = args["input"]
input_path = "./" + input_filename

word_output_filename = args["output_words"]
word_output_path = "./" + word_output_filename

title_output_filename = args["output_titles"]
title_output_path = "./" + title_output_filename

words = []
titles = []
with open(input_path, "r") as input_file, open(word_output_path, "w") as word_output_file, open(title_output_path, "w") as title_output_file:
    for line in input_file:
        title = line.strip().split(' ')
        titles.append(title)
        words += title
    
    words = np.unique(words, return_counts=True)
    vocabulary = list(zip(words[0], words[1]))
    vocabulary.sort(key=lambda elm: -elm[1])
    
    dictionary = []
    for e in range(len(vocabulary)):
        word_output_file.write(vocabulary[e][0] + " " + str(vocabulary[e][1]) + "\n")
        dictionary.append((vocabulary[e][0], e))
        
    dictionary = dict(dictionary)

    for e in range(len(titles)):
        title_indexed = [str(dictionary[word]) for word in titles[e]]
        title_output_file.write((" " . join(title_indexed)) + "\n");

print("Success")
