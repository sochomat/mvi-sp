import os.path
import csv
from preparation_functions import process_title

dataset_file_name = "abcnews-date-text.csv"
dataset_extraction_folder = "data"
dataset_path = dataset_extraction_folder + "/" + dataset_file_name;
processed_data_file_name = "data.txt"

count = 0
if not os.path.isfile(processed_data_file_name):
    with open(dataset_path) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        with open(processed_data_file_name, "w") as processed_data_file:
            for row in reader:
                processed_title = process_title(row[1])
                processed_data_file.write(processed_title + "\n")
                count += 1


print("Count: ")
print(count)
