import nltk
from nltk.tokenize import word_tokenize

def contains_alphanum(string):
    for character in list(string):
        if character.isalnum():
            return True

    return False

def process_title(title):
    processed_title = ""

    tokenized_words = word_tokenize(title)
                    
    '''                
    chunked = nltk.ne_chunk(nltk.pos_tag(tokenized_words), binary=True)
    tokenized_words = []
    for chunk in chunked:
        if type(chunk) == nltk.Tree:
            tokenized_words.append("_".join([token for token, pos in chunk.leaves()]))
        else:
            tokenized_words.append(chunk[0])
    '''

    for e in range(len(tokenized_words)):
        token = tokenized_words[e].lower()

        if token in ["'", '"', "...", "a", "an", "the"]:
            continue

        if (token[0] == "'" or token[0] == '"') and token != "'s":
            token = token[1:]

        if token != "&": 
            if token != "$" and token != "€" and not contains_alphanum(token):
                continue

            if (e == 0 or tokenized_words[e-1] != "&") and processed_title != "" and token != "'s":
                processed_title += " "                           

        processed_title += token

    return processed_title
