import sys
import tensorflow as tf
import numpy as np
import math
import argparse
from preparation_functions import process_title

#Prepare all parameters of the program and the model

parser = argparse.ArgumentParser()

parser.add_argument('--input_words', help='Input file, one word per line', default="words_processed.txt")
parser.add_argument('--input_titles', help='Input file, one indexed title per line', default="titles_processed.txt")
parser.add_argument('--subsample', help='Train the model', default=True)
parser.add_argument('--input_subsampled_titles', help='Input file, one indexed title per line', default="subsampled_titles_processed.txt")
parser.add_argument('--input_initial_titles', help='Input file with titles, without titles eliminated during subsampling', default="initial_titles_processed.txt")
parser.add_argument('--save', help='Save the model after training phase', default=False)
parser.add_argument('--save_to', help='File to save the model to', default="./model/model.ckpt")
parser.add_argument('--restore', help='Restore the model from file', default=False)
parser.add_argument('--restore_from', help='File to restore the model from', default="./model/model.ckpt")
parser.add_argument('--train', help='Train the model', default=True)
parser.add_argument('--train_for', help='Number of episodes to train for', default=1000000)
parser.add_argument('--inference', help='Do the inference stage', default=False)
parser.add_argument('--infer_for', help='Number of episodes to infer for', default=2000)

args = vars(parser.parse_args())

restore_model_path = args["restore_from"]
save_model_path = args["save_to"]
inference_model_path = "./inference_model/inference_model.ckpt"
batch_size = 32
word_embedding_size = 100
title_embedding_size = 100
window_size = 2
allowed_overlap = 1
context_type = "edges"

if context_type == "left":
    window_scope = window_size
elif context_type == "edges":
    window_scope = window_size * 2
combined_embedding_size = word_embedding_size * window_scope + title_embedding_size

learning_rate_par = 8
inference_learning_rate_par = 8
learning_episodes = int(args["train_for"])
inference_episodes = int(args["infer_for"])
num_sampled = 16
loss_function = "nce"
optimizer_type = "adagrad"

perform_subsampling = args["subsample"] == "True" if type(args["subsample"]) == str else args["subsample"]
train = args["train"] == "True" if type(args["train"]) == str else args["train"]
save_model = args["save"] == "True" if type(args["save"]) == str else args["save"]
restore_model = args["restore"] == "True" if type(args["restore"]) == str else args["restore"]
inference = args["inference"] == "True" if type(args["inference"]) == str else args["inference"]

words_validation_set_size = 20
words_validation_set_limit = 1000
words_validation_k_nearest = 8
words_validation_set = np.random.choice(words_validation_set_limit, words_validation_set_size, replace=False)

input_path = "./"

word_input_filename = args["input_words"]
word_input_path = input_path + word_input_filename

title_input_filename = args["input_titles"]
title_input_path = input_path + title_input_filename

subsampled_title_filename = args["input_subsampled_titles"]
subsampled_title_path = input_path + subsampled_title_filename

initial_title_filename = args["input_initial_titles"]
initial_title_path = input_path + initial_title_filename            

def save_titles(titles, file_path):
    with open(file_path, "w") as title_file:
        for title in titles:
            title_file.write((" " . join([str(token) for token in title])) + "\n");

def load_titles(file_path):
    loaded_titles = []
    with open(file_path, "r") as title_file:        
            for line in title_file:
                ids = line.split(" ")
                loaded_titles.append([int(title_id) for title_id in ids])

    return loaded_titles

# Loads and prepares the inputs
words = []
word_counts = []
titles = []
with open(word_input_path, "r") as word_input_file, open(title_input_path, "r") as title_input_file:
    for line in word_input_file:        
        word, word_count = line.split(" ")
        words.append(word)
        word_counts.append(int(word_count))

    for line in title_input_file:
        ids = line.split(" ")
        titles.append([int(title_id) for title_id in ids])

initial_titles = titles

# Performs subsampling of frequent words to prevent them from influencing the model too much
if perform_subsampling:
    subsampled_titles = []
    if restore_model:
        subsampled_titles = load_titles(subsampled_title_path)
        initial_titles = load_titles(initial_title_path)
    else:
        initial_titles = []
        data_words_length = np.sum(np.array(word_counts))
        keeping_probability = np.array([(np.sqrt(word_count / data_words_length / 0.001) + 1) * (0.001 / word_count * data_words_length)  for word_count in word_counts])
        for title in titles:
            subsampled_title = []
            for word in title:
                if np.random.rand() <= keeping_probability[word]:
                    subsampled_title.append(word)
                else:
                    word_counts[word] -= 1

            if len(subsampled_title) > 0:
                subsampled_titles.append(subsampled_title)
                initial_titles.append(title)       
        
        if save_model:
            save_titles(subsampled_titles, subsampled_title_path)
            save_titles(initial_titles, initial_title_path)

    titles = subsampled_titles 
    

words.append("EDGE")
word_counts.append(1)
vocabulary_size = len(words)
titles_count = len(titles)

titles_count += 1
titles.append([len(words) - 1])
initial_titles.append([len(words) - 1])
words_dict = dict([(word[1], word[0]) for word in enumerate(words)])

titles_validation_set_size = 4
titles_validation_k_nearest = 8
titles_validation_set = np.random.choice(titles_count, titles_validation_set_size, replace=False)

def translate_title_into_string(title, words):
    return " ".join([words[word_id] for word_id in title])

def generate_batch(titles, batch_size, window_size, vocabulary_size, context_type, allowed_overlap, restrict_to=None):
    inputs = []
    labels = []

    while len(inputs) < batch_size:
        if restrict_to == None:
            title_id = np.random.randint(0, len(titles))
        else:
            title_id = restrict_to

        title = titles[title_id]

        if context_type == "left":
            right_border = len(title)
        elif context_type == "edges":
            right_border = len(title) - window_size + allowed_overlap

        if window_size >= right_border:
            continue

        for target in range(window_size - allowed_overlap, right_border, 1):
            single_input = [title_id]

            if context_type == "left":
                window_range = range(target - window_size, target)             
            elif context_type == "edges":
                window_range = range(target - window_size, target + window_size + 1, 1)

            for f in window_range:
                if f == target:
                    continue

                if f < 0 or f >= len(title):
                    single_input.append(vocabulary_size - 1)
                else:
                    single_input.append(title[f])
            
            labels.append([title[target]])
            inputs.append(single_input)
            
            if len(inputs) >= batch_size:
                break

    return inputs, labels

def print_k_nearest_titles(title_list_index, initial_titles, titles_similarity_info, titles_list, k, words):
    nearest = (-titles_similarity_info[title_list_index, :]).argsort()[1:k + 1]
    print("Nearest to \"{}\":\n".format(translate_title_into_string(initial_titles[titles_list[title_list_index]], words)))
    for g in range(k):
        print('"' + translate_title_into_string(initial_titles[nearest[g]], words) + '"')
    print("\n")

# Prepares a distribution skewed to favour more frequent words
def create_fixed_unigram(word_counts, model_labels, num_sampled, vocabulary_size):
    probabilities = np.array([(word_count**(0.75)) for word_count in word_counts])
    probabilities /= np.sum(probabilities)

    return tf.nn.fixed_unigram_candidate_sampler(
        true_classes=tf.cast(model_labels, tf.int64),
        num_true=1,
        num_sampled=num_sampled,
        unique=True,
        range_max=vocabulary_size,
        unigrams=probabilities.tolist()
    )

# Create loss function, skewed versions pick more frequent words more frequently as counterexamples 
def create_loss_function(weights, biases, model_labels, context, num_sampled, vocabulary_size, word_counts, loss_function):
    if loss_function == "nce":
        inner_loss = tf.nn.nce_loss(
                        weights=weights,
                        biases=biases,
                        labels=model_labels,
                        inputs=context,
                        num_sampled=num_sampled,
                        num_classes=vocabulary_size
                    )
    elif loss_function == "nce-skewed":
        inner_loss = tf.nn.nce_loss(
                        weights=weights,
                        biases=biases,
                        labels=model_labels,
                        inputs=context,
                        num_sampled=num_sampled,
                        num_classes=vocabulary_size,
                        sampled_values=create_fixed_unigram(word_counts, model_labels, num_sampled, vocabulary_size)
                    )
    elif loss_function == "sampled":
        inner_loss = tf.nn.sampled_softmax_loss(
                        weights=weights,
                        biases=biases,
                        labels=model_labels,
                        inputs=context,
                        num_sampled=num_sampled,
                        num_classes=vocabulary_size
                    )
    elif loss_function == "sampled-skewed":
        inner_loss = tf.nn.sampled_softmax_loss(
                        weights=weights,
                        biases=biases,
                        labels=model_labels,
                        inputs=context,
                        num_sampled=num_sampled,
                        num_classes=vocabulary_size,
                        sampled_values=create_fixed_unigram(word_counts, model_labels, num_sampled, vocabulary_size)
                    )

    return tf.reduce_mean(inner_loss)

# Create context lookup to the text and word vector matrices
def create_context_operations(word_embeddings, title_embeddings, model_inputs, window_scope):
    context_parts = [tf.nn.embedding_lookup(title_embeddings, model_inputs[:, 0])]
    for e in range(1, window_scope + 1):
        context_parts.append(tf.nn.embedding_lookup(word_embeddings, model_inputs[:, e]))
    return tf.concat(context_parts, 1);


graph = tf.Graph()
with graph.as_default():
    # Initialize matrices of embeddings
    word_embeddings = tf.Variable(tf.random_uniform([vocabulary_size, word_embedding_size], -1.0, 1.0))
    title_embeddings = tf.Variable(tf.random_uniform([titles_count, title_embedding_size], -1.0, 1.0))
    
    # Initialize weights
    weights = tf.Variable(tf.truncated_normal([vocabulary_size, combined_embedding_size], stddev=1.0/math.sqrt(combined_embedding_size)))
    biases = tf.Variable(tf.zeros([vocabulary_size]))

    # Prepare placeholders for minibatch inputs and labels for the model
    model_inputs = tf.placeholder(tf.int32, shape=[batch_size, window_scope + 1])
    model_labels = tf.placeholder(tf.int32, shape=[batch_size, 1])

    '''
    Prepare validation operations.
    In this case, validation has to be performed manually, by looking at examples of titles and their nearest neighbours in the vector space and words and their nearest neighbours.
    '''
    words_validation_dataset = tf.constant(words_validation_set, dtype=tf.int32)
    words_embedding_normalization = tf.sqrt(tf.reduce_sum(tf.square(word_embeddings), 1, keepdims=True))
    words_normalized_embeddings = word_embeddings / words_embedding_normalization
    words_validation_embeddings = tf.nn.embedding_lookup(words_normalized_embeddings, words_validation_dataset)
    words_similarity = tf.matmul(words_validation_embeddings, words_normalized_embeddings, transpose_b=True)

    titles_validation_dataset = tf.constant(titles_validation_set, dtype=tf.int32)
    titles_embedding_normalization = tf.sqrt(tf.reduce_sum(tf.square(title_embeddings), 1, keepdims=True))
    titles_normalized_embeddings = title_embeddings / titles_embedding_normalization
    titles_validation_embeddings = tf.nn.embedding_lookup(titles_normalized_embeddings, titles_validation_dataset)
    titles_similarity = tf.matmul(titles_validation_embeddings, titles_normalized_embeddings, transpose_b=True)

    # Basically the same as validation, but used for inference stage
    if inference:
        titles_inference_list = tf.constant([titles_count - 1], dtype=tf.int32)
        titles_inference_embedding = tf.nn.embedding_lookup(titles_normalized_embeddings, titles_inference_list)
        titles_inference_similarity = tf.matmul(titles_inference_embedding, titles_normalized_embeddings, transpose_b=True)

    context = create_context_operations(word_embeddings, title_embeddings, model_inputs, window_scope)
    loss = create_loss_function(weights, biases, model_labels, context, num_sampled, vocabulary_size, word_counts, loss_function)

    # Create training optimizer
    if optimizer_type == "adagrad":
        optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate_par).minimize(loss)
    elif optimizer_type == "adadelta":
        optimizer = tf.train.AdadeltaOptimizer(learning_rate=learning_rate_par).minimize(loss)
    elif optimizer_type == "sgd":
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate_par).minimize(loss)

    session = tf.Session(graph=graph)
    init = tf.global_variables_initializer()
    if save_model or restore_model:
        saver = tf.train.Saver()

    session.run(init)        
    
    switched = False
    with session.as_default():
        if restore_model:
            saver.restore(session, restore_model_path)

        # Perform the training stage
        if train:
            title_embedding_output = np.random.randint(len(titles))
            average_loss = 0
            for e in range(learning_episodes):
                inputs, labels = generate_batch(titles, batch_size, window_size, vocabulary_size, context_type, allowed_overlap)
                feed_dict = {model_inputs: inputs, model_labels: labels}
                _, curr_loss = session.run([optimizer, loss], feed_dict=feed_dict)
                average_loss += curr_loss
                if (e + 1) % 1000 == 0:
                    print("Loss at episode {} is {}".format(e+1, average_loss / 1000))
                    average_loss = 0

                if (e + 1) % 50000 == 0:
                    print("\n\n\n")
                    words_similarity_info = words_similarity.eval()
                    for f in range(words_validation_set_size):
                        nearest = (-words_similarity_info[f, :]).argsort()[1:words_validation_k_nearest + 1]
                        print("Nearest to {}: ".format(words[words_validation_set[f]]), end='')
                        for g in range(words_validation_k_nearest):
                            print(words[nearest[g]], end=', ')
                        print()

                    titles_similarity_info = titles_similarity.eval()
                    print("\n\n\n")
                    for f in range(titles_validation_set_size):
                        print_k_nearest_titles(f, initial_titles, titles_similarity_info, titles_validation_set, titles_validation_k_nearest, words)
                    print("\n\n\n")

        if save_model:  
            save_path = saver.save(session, save_model_path)
            print("Model saved in path: %s" % save_path)
        

        # Perform the inference stage
        if inference:
            # Create inference optimizer (only optimizes title embeddings)
            if optimizer_type == "adagrad":
                inference_optimizer_inner = tf.train.AdagradOptimizer(learning_rate=inference_learning_rate_par)
            elif optimizer_type == "adadelta":
                inference_optimizer_inner = tf.train.AdadeltaOptimizer(learning_rate=inference_learning_rate_par)
            elif optimizer_type == "sgd":
                inference_optimizer_inner = tf.train.GradientDescentOptimizer(learning_rate=inference_learning_rate_par)
            inference_optimizer = inference_optimizer_inner.minimize(loss, var_list=[title_embeddings])
            
            session.run(tf.variables_initializer([inference_optimizer_inner.get_slot(title_embeddings, name) for name in inference_optimizer_inner.get_slot_names()]))

            saver.save(session, inference_model_path)
            print("\n\n\nInference stage")
            print("---------------")
            while input("Do you want to get matches to a title? (yes|no): ") == "yes":
                text = input("Write a title to get {} nearest matches to:\n".format(words_validation_k_nearest))
                
                saver.restore(session, inference_model_path)

                text = process_title(text)
                words_in_title = text.split(" ")
                
                # Some of the title's words might not be in the vocabulary. It is not possible to properly train them from one example, and they are therefore cut from the title.
                cut_words = []
                title = []
                for word in words_in_title:
                    if word in words_dict:
                        title.append(words_dict[word])
                    else:
                        cut_words.append(word)
                
                if len(cut_words) > 0:
                    print("Some words haven't been in vocabulary:")
                    print(np.unique(cut_words))
                    print("They were cut from the title and play no role in the inference")

                if len(title) < window_scope + 1:
                    print("The title you entered is too short. Inference needs at least {} recognized words.".format(window_scope + 1))
                    continue

                titles[titles_count - 1] = title
                initial_titles[titles_count - 1] = title
                title_embedding = tf.Variable(tf.random_uniform([1, title_embedding_size], -1.0, 1.0))
                session.run(title_embedding.initializer)
                assigning = tf.assign(title_embeddings, tf.concat([tf.slice(title_embeddings, [0, 0], [titles_count - 1, title_embedding_size]), title_embedding], 0))                
                session.run(assigning)

                for e in range(inference_episodes):
                    inputs, labels = generate_batch(titles, batch_size, window_size, vocabulary_size, context_type, allowed_overlap, titles_count - 1)
                    feed_dict = {model_inputs: inputs, model_labels: labels}
                    _, curr_loss = session.run([optimizer, loss], feed_dict=feed_dict)

                print_k_nearest_titles(0, initial_titles, titles_inference_similarity.eval(), [titles_count - 1], titles_validation_k_nearest, words)
